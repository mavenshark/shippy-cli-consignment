# first build process for building the app
FROM golang:1.12.7-alpine as builder
LABEL stage=intermediate

RUN apk update && apk upgrade && \
    apk add --no-cache git

RUN mkdir /app
WORKDIR /app
#WORKDIR /go/src/github.com/DidierStockmans/shippy-service-consignment/consignment-cli

COPY . .

#RUN go get -u github.com/golang/dep/cmd/dep
#RUN dep init && dep ensure
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o shippy-cli-consignment

FROM alpine:3.10.1

RUN apk --no-cache add ca-certificates
RUN mkdir /app
WORKDIR /app

COPY consignment.json /app/consignment.json
#COPY --from=builder /go/src/github.com/DidierStockmans/shippy-service-consignment/consignment-cli/consignment-cli .
COPY --from=builder /app/shippy-cli-consignment .

CMD ["./shippy-cli-consignment"]